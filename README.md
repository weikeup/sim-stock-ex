# SimStockEx <sub>Stock Exchange Simulator

This project is built with Spring Boot and the following modules:
- Spring Web
- Spring Security
- Spring Data Jpa
- Lombok
- Thymeleaf
- MariaDB
- Mockito (for test)
- AssertJ (for test)