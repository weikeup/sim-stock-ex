package cc.weikeup.simstockex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimStockExApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimStockExApplication.class, args);
    }

}
